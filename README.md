# Private Package Management for Scientific Research with Julia

## Intro

These are some personal notes on how to manage and develop personal, i.e. unregistered, Julia packages. My goals here are to:

1. Use personal, generally private version-controlled Julia packages to store most of my research functions.
2. Ensure that old experiment files still run by linking them to older versions of packages.
3. Be able to easily share both current and older versions of experiment files and the correct versions of the related packages with collaborators.

The following instructions are written specifically for me and my standard workflow, but should be easily adaptable to a different environment.

## Developing local packages

I like to keep all of my personal packages easily accessible and separate from the regular Julia packages, so I store them in the directory `~/lanl/code`, and in my `startup.jl` file I have

```julia
ENV["JULIA_PKG_DEVDIR"] = "/Users/golden/lanl/code"
```

### Initiating a new package

Here are the steps that I follow to start developing a new package, `TestPkg`.

1. Generate the package via Julia:

```julia
cd ~/lanl/code
julia
] generate TestPkg
```

(The Julia docs recommend using [https://github.com/invenia/PkgTemplates.jl](https://github.com/invenia/PkgTemplates.jl) to generate the files for a new package, but so far I haven’t found the added functionality necessary for my use cases.)

1. Create a new project `TestPkg.jl` in my private gitlab account.
    - Don’t initialize repository with a README
    - Change the project slug from `testpkg.jl` to `TestPkg.jl`
2. Push the `TestPkg.jl` folder

```bash
cd ~/lanl/code/TestPkg
git init --initial-branch=main
git remote add origin git@gitlab.com:johngolden/TestPkg.jl.git
git add .
git commit -m "Initial commit"
git push -u origin main
```

1. Now add the package to our main Julia environment in the `dev` mode

```bash
julia
] dev https://gitlab.com/johngolden/TestPkg.jl.git
```

1. An optional and very nit-picky thing I like to do is have all the Julia packages in my `code` directory end in  `.jl` so that it’s clear what is a Julia package instead of e.g. python environments and other code I have stored there. The fix for this is to add the `.jl` to the `code/TestPkg` directory and then change the path for `TestPkg` in the `Manifest.toml`file in my main Julia environment to `path = "/Users/golden/lanl/code/TestPkg.jl"`.
2. Now we should be set for development and local use in the main Julia environment. For example, running the following code in Julia should result in an output of “Hello World!”

```julia
import TestPkg
TestPkg.greet()
```

### Making changes and updating versions

Let’s do some development on `TestPkg`. Making sure we are `using Revise.jl`

1.  Let’s change `TestPkg.jl/src/TestPkg.jl` so that the `greet()` function is

```julia
greet() = print("Hello World! This is v0.2.0")
```

1. After testing the updated code in our active Julia session, we decide to commit the changes. If this was just a minor fix to an existing version, we would just do a basic `git add, commit, push` workflow. But since we have made significant changes, we need to update the version. This is done by changing `TestPkg.jl/Project.toml` file so that we have

```julia
version = "0.2.0"
```

1. Now we can push this new version

```julia
git add .
git commit -m "v0.2.0"
git push
```

1. Note that if you run `Pkg.status()` you’ll see that Julia hasn’t picked up on the new version number yet (even though the actual functionality, i.e. `greet()`, has been updated). This is fixed by just running `Pkg.update()`, which gives the output

```julia
~ TestPkg v0.1.0 `~/lanl/code/TestPkg.jl` ⇒ v0.2.0 `~/lanl/code/TestPkg.jl`
```

## Using older versions of local packages

A common scenario for me is to have a file `research.jl` where I start doing some research using the functionality of `TestPkg.jl`. Based on what I’ve learned with `research.jl`, I decide I want to make some improvements to `TestPkg.jl` and start a fresh line of research in `new_research.jl`. However, the changes I want to make will make `research.jl` no longer run correctly. The “easiest” way to do this, at least in the short term, would be to create a new package called `TestPkg_v2.jl` and use that for `new_research.jl`. But as projects get bigger and grow in complexity it quickly becomes a real pain to keep track of all this, and it makes sharing with collaborators messier.

The better long-term solution, at least for me, is usually to “soft” archive `research.jl`. Here is what that generally looks like.

1. This isn’t always necessary, but for the sake of this example let’s rename `research.jl` to `old_research.jl` and move it into a folder called `old_research/`.
2. Activate a new Julia environment in `old_research/`:

```julia
cd old_research
julia
] activate .
```

1. Now we need to add  `v0.1.0` of `TestPkg.jl` to this environment. We do this by attaching the `v0.1.0` commit SHA to the end of the `add` command (you can use this approach to follow certain branches as well, but I haven’t tested this):

```julia
add https://gitlab.com/johngolden/TestPkg.jl.git#5f3d299d12e91c787dd99c93f3b08d1da32aec93
```

1. You’ll also need to `add` whatever other packages `old_research.jl` depends on.
2. Now if `old_research.jl` simply contains the following code, we should get

```julia
import Pkg
Pkg.activate(".")
Pkg.instantiate() # only necessary the first time this environment is activated

import TestPkg

TestPkg.greet() # = "Hello World!"
Pkg.status() # = [5d290c27] TestPkg v0.1.0 `https://gitlab.com/johngolden/TestPkg.jl.git#5f3d299`
```
Note that the `Pkg.instantiate()` line is quite important for the following subtle reason. Let's say you have the latest and greatest version of `TestPkg.jl`, e.g. `v0.4.0`, installed in your main Julia environment, and do *not* have any earlier versions installed. Then if you run the above code without instantiate, Julia will -- for some reason! maybe a bug? -- load `TestPkg v0.3.0` and not give you any errors or anything. Only if you run `Pkg.instantiate()` after activating the environment, or already have a copy of `v0.1.0` in your main environment, will you get the correct `TestPkg v0.1.0` loaded into the environment for the old code. Seems a bit wonky to me. If you do run `Pkg.status()` in this scenario it will at least warn you that `v0.1.0` isn't downloaded, but that seems like not enough warning to me.


## Sharing with collaborators

Let's look at two scenarios: sharing code with a collaborator that

1. depends on an actively developed unregistered package,
2. depends on a specific version of an unregistered package.

### Sharing actively developed code

Our collaborator Alice can start using `TestPkg.jl` via either `add` or `dev` + the git address.

- `add` is best when Alice doesn’t plan to make any changes to `TestPkg.jl`. Specifically, Alice can get new versions of `TestPkg.jl` via the standard `Pkg.update()` command. If she finds that she does want to make changes, she can `dev TestPkg` and push changes.  Note that the standard `free` command doesn’t work for unregistered packages, so if Alice wants to go back to the traditional environment she will need to `rm TestPkg` and then `add` the git repo again.
- `dev` is for when Alice is likely to make changes. In this case, if I push a new version of `TestPkg.jl` she will *not* see it via `Pkg.update()`, but will instead have to navigate to her `.julia/dev/` folder (or wherever she has set her `JULIA_PKG_DEVDIR`) and do a `git pull` and then run `Pkg.update()`.

### Sharing code tied to an old version of an unregistered package

Fortunately, the approach described above in the `old_research.jl` file should work as-is for Alice. That is, if you share the code with Alice along with the `Manifest` and `Project` `.toml` files,  then the commands

```julia
import Pkg
Pkg.activate(".")
Pkg.instantiate() # only necessary the first time this environment is activated
```

are sufficient to get `Pkg` to download and build the correct environment for `old_research.jl`.
